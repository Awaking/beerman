<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include(getenv('OPENSHIFT_REPO_DIR').'RequestConstants.php');

class BeerMan {

    public $email;
    public $password;
    private $curlInstance;

    //Common costructor
    function __construct(){ 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    } 

    //Default constructor
    function __construct2($email, $password) { 
        $this->email=$email;
        $this->password=$password;
    }

    //Get file name to store cookies
    function cookieFileName(){
        $validEmail = str_replace("@", "_AT_", $this->email);
        $validEmail = str_replace(".", "_", $validEmail);
        return $validEmail . ".txt";
    }

    //User agent depends on email (always constant for every email)
    function userAgent(){
        $seed = NULL;
        for ($i = 0; $i < strlen($this->email); $i++) 
            $seed += ord($this->email[$i]); 

        srand($seed);
        $randomNumber=rand(0, count(RequestConstants::$USER_AGENTS)-1);
        return RequestConstants::$USER_AGENTS[$randomNumber];

    }

    function printCurlResponse($curl, $response){
        if(!$response) {
          die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } else {
          echo "Response HTTP Status Code : " . curl_getinfo($curl, CURLINFO_HTTP_CODE)."<br>";
          echo "\nResponse HTTP Body : " . $response;
        }
    }

    function initCurl($url, $type, $fields){
        //Init curl instance options
        $this->curlInstance = curl_init();

        // Set options
        curl_setopt($this->curlInstance, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($this->curlInstance, CURLOPT_POST, 1);
        //Enable headers        
        curl_setopt($this->curlInstance, CURLOPT_HEADER, 1);
        //Enable redirects
        curl_setopt($this->curlInstance, CURLOPT_FOLLOWLOCATION, 1);
        //Set file to store cookies
        curl_setopt($this->curlInstance, CURLOPT_COOKIEJAR, getenv('OPENSHIFT_REPO_DIR').$this->cookieFileName());
        curl_setopt($this->curlInstance, CURLOPT_COOKIEFILE, getenv('OPENSHIFT_REPO_DIR').$this->cookieFileName());
        //Set email depending random user agent
        curl_setopt($this->curlInstance, CURLOPT_USERAGENT, $this->userAgent());
        // Set url
        curl_setopt($this->curlInstance, CURLOPT_URL, $url);
        // Set method
        curl_setopt($this->curlInstance, CURLOPT_CUSTOMREQUEST, $type);
        // Set body
        curl_setopt($this->curlInstance, CURLOPT_POSTFIELDS, $fields);
    }

    function loginRequest($printPage){
        //Init curl instance options
        // Create body
        $body = [          
          "log_email" => $this->email,
          "pass" => $this->password,
          ];
        $this->initCurl(RequestConstants::LOGIN_URL, RequestConstants::LOGIN_TYPE, $body);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonus5minRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_5_MIN_URL, 
            RequestConstants::BONUS_5_MIN_TYPE, 
            RequestConstants::$BONUS_5_MIN_BODY);

        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonus10minRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_10_MIN_URL, 
            RequestConstants::BONUS_10_MIN_TYPE, 
            RequestConstants::$BONUS_10_MIN_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonus15minRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_15_MIN_URL, 
            RequestConstants::BONUS_15_MIN_TYPE, 
            RequestConstants::$BONUS_15_MIN_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonus30minRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_30_MIN_URL, 
            RequestConstants::BONUS_30_MIN_TYPE, 
            RequestConstants::$BONUS_30_MIN_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }    

    function bonus60minRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_60_MIN_URL, 
            RequestConstants::BONUS_60_MIN_TYPE, 
            RequestConstants::$BONUS_60_MIN_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonusDailyRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_DAILY_URL, 
            RequestConstants::BONUS_DAILY_TYPE, 
            RequestConstants::$BONUS_DAILY_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function bonusVivodRequest($printPage){
        $this->initCurl(RequestConstants::BONUS_VIVOD_URL, 
            RequestConstants::BONUS_VIVOD_TYPE, 
            RequestConstants::$BONUS_VIVOD_BODY);
        //MAKE THE REQUEST
        $curlResponse = curl_exec($this->curlInstance);
        if($printPage) 
            BeerMan::printCurlResponse($this->curlInstance, $curlResponse);
        curl_close($this->curlInstance);
        return BeerMan::handeResult($curlResponse);
    }

    function handeResult($curlResponse){

        if(strpos($curlResponse, RequestConstants::ANSWER_POSITIVE)){
            return BONUS_RESULT_OK;
        } else if(strpos($curlResponse, RequestConstants::ANSWER_NEGATIVE)){
            return BONUS_RESULT_FAILED;
        } else {
            return BONUS_RESULT_ERROR;
        }
    }

    static function resultToString($result){
        if($result==BONUS_RESULT_OK)
            return "OK";
        else if ($result==BONUS_RESULT_FAILED)
            return "EARLY";
        else 
            return "ERROR";
    }
}



?>